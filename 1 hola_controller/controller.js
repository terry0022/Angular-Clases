var app = angular.module('myFirstApp', []);

app.controller('firstCtrl', ['$scope',"$http", function ($scope,$http) {
	$scope.nombre = "Terry0022";
	$scope.compan = "Jarboss";
	$scope.nuevoComentario = {};
	$scope.comentarios = [
		{
			comentario: "primero",
			username: "first"
		},
		{
			comentario: "segundo",
			username: "second"
		}
	];
	$scope.agregarComentario = function(){
		$scope.comentarios.push($scope.nuevoComentario);
		$scope.nuevoComentario = {};
	};
}]);

app.controller('secondCtrl', ['$scope', function ($scope) {
	$scope.letter = "Hola mundo";
}]);