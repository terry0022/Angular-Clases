var app = angular.module('ToDoList', ['LocalStorageModule']);
app.controller('ToDoCtrl', ['$scope','localStorageService', function ($scope,LocalS) {
	if(LocalS.get("angular-todolist")){
		$scope.todo = LocalS.get("angular-todolist");
	}else{
		$scope.todo = [];
	}
	/*
		{
			descripcion: 'Terminar el curso de angular',
			fecha: '03-03-15 2:00pm'
		}
	*/
	$scope.$watchCollection('todo',function(newValue,oldValue){
		LocalS.set("angular-todolist",$scope.todo);
	});
	$scope.addActv = function(){
		$scope.todo.push($scope.newActv);
		$scope.newActv = {};
	};
}]);
